package com.jfirer.mvc.boot;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Documented
public @interface AppInfo
{
    public String appName();
    
    public String prefix() default "web";
    
    public int port() default 80;
    
}
