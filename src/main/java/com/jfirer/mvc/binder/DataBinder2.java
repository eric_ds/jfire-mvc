package com.jfirer.mvc.binder;

import java.util.Map;

public interface DataBinder2
{

    /**
     * 将servlet标准生成paramMap绑定到创建的对象上，绑定完成后返回该对象本身
     *
     * @param map
     * @return
     */
    Object bindMulti(Map<String, String[]> map);

    /**
     * bindMulti方法的简化版本，支持Map<String, String>形式的入参
     *
     * @param map
     * @return
     */
    Object bind(Map<String, String> map);

//    /**
//     * 将name代表的路径在寻找合适的映射，并且创建对应的容器类实例container。
//     *
//     * @param name
//     * @param value
//     * @return
//     */
//    Object bind(String name, String[] value);

//    /**
//     * a)如果当前对象是容器类型，比如自定义对象、数组、List，将name代表的路径与自身元素（自定义对象中的属性、数组中指定下标的元素、List中的元素）的路径进行前缀匹配，如果吻合，则将参数传递到属性的DataBinder中进行处理。
//     *  1）如果属性是容器类型，则将属性的实例作为container参数传入。
//     * b)如果当前对象是值类型，比如基本类型、对应的包装类、String，并且name代表的路径与自身的前缀相等，则使用value转化为合适的值。
//     *
//     * 将name代表的路径寻找合适的映射，并且将value代表的字符串转化为合适的值类型。
//     * 如果container为null，则存在几种可能：
//     * 1）当前映射路径对应的值类型不存在容器类
//     * 将name代表的路径在寻找合适的映射，并且将值绑定到容器类实例container上。绑定完成后返回容器类实例。
//     * 如果传入的容器类实例不合适，比如数组的长度不合适，放不下的情况，则创建新的容器类实例，并且返回。
//     *
//     * @param name
//     * @param value
//     * @param container
//     */
//    Object bind(String name, String[] value, Object container);

//    /**
//     * bindMulti方法的简化版本，支持String 类型的value
//     *
//     * @param name
//     * @param value
//     * @param container
//     */
//    Object bind(String name, String value, Object container);

//    /**
//     * bindMulti方法的简化版本，支持String 类型的value
//     *
//     * @param name
//     * @param value
//     */
//    Object bind(String name, String value);
}
