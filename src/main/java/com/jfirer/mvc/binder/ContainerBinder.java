package com.jfirer.mvc.binder;

public interface ContainerBinder extends DataBinder2
{
    /**
     * 检查自身元素的路径，是否与name相等；如果是的话，将value的值转化为合适的类型，并且设置到容器类中实例,完成后返回容器类实例。如果入参容器类实例不存在，则创建一个实例。
     * 注意：返回的容器类实例可能与入参容器类实例不同，这种情况下，上层容器类需要将新返回的容器类实例作为元素，丢弃掉原本入参的容器类实例。这种情况会出现在容器类为数组的情况下，经过新的
     * 数值转化后，数组扩容了，原本的数组实例就需要被丢弃。
     * @param name
     * @param value
     * @param mayExistContainer
     * @return
     */
    Object bind(String name, String[] value, Object mayExistContainer);

    Object bind(String name, String value, Object mayExistContainer);
}
