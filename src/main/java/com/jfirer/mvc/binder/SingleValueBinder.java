package com.jfirer.mvc.binder;

public interface SingleValueBinder extends DataBinder2
{
    /**
     * 如果传入的name与自身的前缀路径相等，则将value转化为合适的值并且返回
     * @param name
     * @param value
     * @return
     */
    Object bind(String name, String value);

    Object bind(String name, String[] value);
}
