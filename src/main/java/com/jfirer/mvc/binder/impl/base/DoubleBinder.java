package com.jfirer.mvc.binder.impl.base;

public class DoubleBinder extends AbstractSingleValue
{

    public DoubleBinder(String prefix)
    {
        super(prefix);
    }

    @Override
    protected Object bind(String value)
    {
        return Double.valueOf(value);
    }
}
