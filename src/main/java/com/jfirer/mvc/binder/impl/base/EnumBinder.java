package com.jfirer.mvc.binder.impl.base;

public class EnumBinder extends AbstractSingleValue
{
    private String prefix;
    private Class  ckass;

    public EnumBinder(String prefix,Class ckass)
    {
        super(prefix);
        this.ckass = ckass;
    }

    @Override
    protected Object bind(String value)
    {
        return Enum.valueOf(ckass, value);
    }
}
