package com.jfirer.mvc.binder.impl.base;

public class BooleanBinder extends AbstractSingleValue
{

    public BooleanBinder(String prefix)
    {
        super(prefix);
    }

    @Override
    protected Object bind(String value)
    {
        return Boolean.valueOf(value);
    }
}
