package com.jfirer.mvc.binder.impl.base;

import com.jfirer.mvc.binder.SingleValueBinder;

import java.util.Map;

public abstract class AbstractSingleValue implements SingleValueBinder
{
    protected String prefix;

    public AbstractSingleValue(String prefix)
    {
        this.prefix = prefix;
    }

    @Override
    public Object bind(String name, String value)
    {
        return bind(value);
    }

    protected abstract Object bind(String value);

    @Override
    public Object bind(String name, String[] value)
    {
        return bind(value[0]);
    }

    @Override
    public Object bindMulti(Map<String, String[]> map)
    {
        String[] value = map.get(prefix);
        if (value == null)
        {
            return null;
        }
        else
        {
            return bind(value[0]);
        }
    }

    @Override
    public Object bind(Map<String, String> map)
    {
        String value = map.get(prefix);
        if (value == null)
        {
            return null;
        }
        else
        {
            return bind(value);
        }
    }
}
