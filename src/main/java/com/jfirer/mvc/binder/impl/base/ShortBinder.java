package com.jfirer.mvc.binder.impl.base;

public class ShortBinder extends AbstractSingleValue
{
    public ShortBinder(String prefix)
    {
        super(prefix);
    }

    @Override
    protected Object bind(String value)
    {
        return Short.valueOf(value);
    }
}
