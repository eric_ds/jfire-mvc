package com.jfirer.mvc.binder.impl.base;

public class LongBinder extends AbstractSingleValue
{

    public LongBinder(String prefix)
    {
        super(prefix);
    }

    @Override
    protected Object bind(String value)
    {
        return Long.valueOf(value);
    }
}
