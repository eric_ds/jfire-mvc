package com.jfirer.mvc.binder.impl.base;

public class ByteBinder extends AbstractSingleValue
{
    public ByteBinder(String prefix)
    {
        super(prefix);
    }

    @Override
    protected Object bind(String value)
    {
        return Byte.valueOf(value);
    }
}
