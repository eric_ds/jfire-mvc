package com.jfirer.mvc.binder.impl.base;

public class StringBinder extends AbstractSingleValue
{

    public StringBinder(String prefix)
    {
        super(prefix);
    }

    @Override
    protected Object bind(String value)
    {
        return value;
    }
}
