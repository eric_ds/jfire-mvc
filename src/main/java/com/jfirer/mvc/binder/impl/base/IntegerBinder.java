package com.jfirer.mvc.binder.impl.base;

public class IntegerBinder extends AbstractSingleValue
{

    public IntegerBinder(String prefix)
    {
        super(prefix);
    }

    @Override
    protected Object bind(String value)
    {
        return Integer.valueOf(value);
    }
}
