package com.jfirer.mvc.binder.impl.base;

public class FloatBinder extends  AbstractSingleValue
{

    public FloatBinder(String prefix)
    {
        super(prefix);
    }

    @Override
    protected Object bind(String value)
    {
        return Float.valueOf(value)
                ;
    }
}
