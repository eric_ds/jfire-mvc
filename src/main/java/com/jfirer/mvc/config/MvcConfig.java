package com.jfirer.mvc.config;

import com.jfirer.jfire.core.ApplicationContext;
import com.jfirer.jfire.core.inject.notated.PropertyRead;
import com.jfirer.jfire.core.prepare.ContextPrepare;
import com.jfirer.jfire.core.prepare.annotation.Import;

import javax.annotation.Resource;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Resource
public class MvcConfig
{
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Import(HotDevImportSelector.class)
    public static @interface EnableHotDev
    {
        /**
         * 需要热加载的包，以逗号区隔
         *
         * @return
         */
        String reloadPackages();

        /**
         * 需要排除的包，以逗号区隔
         *
         * @return
         */
        String excludePackages();

        /**
         * 需要热加载的类的路径根路径
         *
         * @return
         */
        String reloadPath();

        /**
         * 监控路径，该路径内容发生变化，触发热加载
         *
         * @return
         */
        String monitorPath();

        /**
         * 开启热加载，默认为true
         *
         * @return
         */
        boolean enable() default true;
    }

    public static class HotDevImportSelector implements ContextPrepare
    {

        public ApplicationContext.NeedRefresh prepare(ApplicationContext jfireContext)
        {
//			environment.putProperty("monitorPath", enableHotDev.monitorPath());
//			environment.putProperty("reloadPath", enableHotDev.reloadPath());
//			environment.putProperty("reloadPackages", enableHotDev.reloadPackages());
//			environment.putProperty("excludePackages", enableHotDev.excludePackages());
//			environment.putProperty("hotdev", String.valueOf(enableHotDev.enable()));
            return ApplicationContext.NeedRefresh.NO;
        }

        public int order()
        {
            return 0;
        }
    }

    @PropertyRead("encode")
    private String  encode = "UTF-8";
    @PropertyRead("hotdev")
    private boolean hotdev = false;
    @PropertyRead("monitorPath")
    private String  monitorPath;
    @PropertyRead("reloadPath")
    private String  reloadPath;
    @PropertyRead("reloadPackages")
    private String  reloadPackages;
    @PropertyRead("excludePackages")
    private String  excludePackages;

    public String getExcludePackages()
    {
        return excludePackages;
    }

    public void setExcludePackages(String excludePackages)
    {
        this.excludePackages = excludePackages;
    }

    public String getEncode()
    {
        return encode;
    }

    public void setEncode(String encode)
    {
        this.encode = encode;
    }

    public boolean isHotdev()
    {
        return hotdev;
    }

    public void setHotdev(boolean hotswap)
    {
        this.hotdev = hotswap;
    }

    public String getMonitorPath()
    {
        return monitorPath;
    }

    public void setMonitorPath(String monitorPath)
    {
        this.monitorPath = monitorPath;
    }

    public String getReloadPath()
    {
        return reloadPath;
    }

    public void setReloadPath(String reloadPath)
    {
        this.reloadPath = reloadPath;
    }

    public String getReloadPackages()
    {
        return reloadPackages;
    }

    public void setReloadPackages(String reloadPackages)
    {
        this.reloadPackages = reloadPackages;
    }
}
