package com.jfirer.mvc.util;

import com.jfirer.mvc.annotation.Document;
import com.jfirer.mvc.annotation.RequestMapping;
import com.jfirer.mvc.core.action.Action;
import com.jfirer.mvc.core.action.ActionInitListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractReportMdActionListener implements ActionInitListener
{
	private static final Logger	logger	= LoggerFactory.getLogger(AbstractReportMdActionListener.class);
	private String				pattarn	= "\r\n"														//
	        + "|请求地址|{}|\r\n"																			//
	        + "|请求方法|{}|\r\n"																			//
	        + "|结果类型|{}|\r\n"																			//
	        + "|方法说明|{}|\r\n"																			//
	        + "|类方法签名|{}|\r\n";
	
	@Override
	public void init(Action action)
	{
		if (filter(action))
		{
			String doc;
			if (action.getMethod().isAnnotationPresent(Document.class))
			{
				doc = action.getMethod().getAnnotation(Document.class).value();
			}
			else
			{
				doc = "无";
			}
			for (RequestMapping.RequestMethod requestMethod : action.getRequestMethods())
			{
				logger.debug(pattarn, //
				        action.getRequestUrl(), //
				        requestMethod.name(), //
				        action.getViewRender().renderType(), //
				        doc, //
				        action.getMethod().toGenericString());
			}
		}
	}
	
	protected abstract boolean filter(Action action);
}
