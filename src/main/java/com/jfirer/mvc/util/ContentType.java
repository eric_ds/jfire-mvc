package com.jfirer.mvc.util;

public interface ContentType
{
    public static final String JPG    = "image/jpeg";
    public static final String HTML   = "text/html";
    public static final String JSON   = "application/Json";
    public static final String STRING = "text/plain";
    public static final String STREAM = "application/octet-stream";
}
