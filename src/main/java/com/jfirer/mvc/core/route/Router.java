package com.jfirer.mvc.core.route;

import com.jfirer.mvc.core.action.Action;

import javax.servlet.http.HttpServletRequest;

public interface Router
{
    boolean match(HttpServletRequest request);

    void bindAction(Action action);

    Action bindedAction();
}
