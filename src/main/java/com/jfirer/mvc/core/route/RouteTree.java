package com.jfirer.mvc.core.route;

import javax.servlet.http.HttpServletRequest;

public interface RouteTree
{
    /**
     * 对url进行路由匹配，并且获得匹配节点（如果存在的话）。 在这个过程中，如果路径中存在参数值，则会提取出来放入resolver中
     * 
     * @param url
     * @param request
     * @return
     */
    Router route(String url, HttpServletRequest request);
    
    Router resolve(String name, String url, String httpMethod, String... headerRules);
    
    void finishResolve();
}
