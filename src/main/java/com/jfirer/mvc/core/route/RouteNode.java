package com.jfirer.mvc.core.route;

import javax.servlet.http.HttpServletRequest;

public interface RouteNode
{
    Router route(String[] paths, int index, HttpServletRequest request);
    
    /**
     * 解析一个url路径规则
     * 
     * @param paths 路径规则切分后的数组
     * @param index 当前解析的位置下标
     * @param httpMethod 方法路径规则对应的http方法
     * @param headerRules
     *            该路径规则对应的header规则。如果值是一个字符串，则意味着header中必须包含指定的header，如果是k=v格式，则意味着不仅要包含，而且值必须等于指定的值
     * @return
     */
    Router resolve(String name, String[] paths, int index, String httpMethod, String[] headerRules);
    
    void finishResolve();
}
