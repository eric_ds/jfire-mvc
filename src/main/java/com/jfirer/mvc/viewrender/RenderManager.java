package com.jfirer.mvc.viewrender;

import com.jfirer.jfire.core.inject.notated.MapKeyMethodName;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Resource
public class RenderManager
{
    @Resource
    @MapKeyMethodName("renderType")
    private Map<String, ViewRender> renders = new HashMap<String, ViewRender>();
    
    public ViewRender get(String type)
    {
        ViewRender render = renders.get(type);
        return render;
    }
}
