package com.jfirer.mvc.viewrender.impl;

import com.jfirer.mvc.viewrender.DefaultResultType;
import com.jfirer.mvc.viewrender.ViewRender;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Resource
public class HtmlRender implements ViewRender
{
    
    @Override
    public void render(HttpServletRequest request, HttpServletResponse response, Object result) throws Throwable
    {
        request.getRequestDispatcher((String) result).forward(request, response);
    }
    
    @Override
    public String renderType()
    {
        return DefaultResultType.Html;
    }
}
