package com.jfirer.mvc.resolver;

public abstract class AbstractParamResolver implements ParamResolver
{
    protected String paramName;

    public AbstractParamResolver(String paramName)
    {
        this.paramName = paramName;
    }

    @Override
    public String paramName()
    {
        return paramName;
    }

   protected static Object toValue(Class type, String value)
    {
        if (type == Integer.class)
        {
            return Integer.valueOf(value);
        }
        else if (type == Byte.class)
        {
            return Byte.valueOf(value);
        }
        else if (type == Short.class)
        {
            return Short.valueOf(value);
        }
        else if (type == Long.class)
        {
            return Long.valueOf(value);
        }
        else if (type == Boolean.class)
        {
            return Boolean.valueOf(value);
        }
        else if (type == Float.class)
        {
            return Float.valueOf(value);
        }
        else if (type == Double.class)
        {
            return Double.valueOf(value);
        }
        else if (type == Character.class)
        {
            return value.charAt(0);
        }
        else if (type == String.class)
        {
            return value;
        }
        else
        {
            throw new IllegalArgumentException();
        }
    }
}
