package com.jfirer.mvc.resolver.impl;

import com.jfirer.baseutil.StringUtil;
import com.jfirer.mvc.resolver.AbstractParamResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SingleValueResolver extends AbstractParamResolver
{

    private Class ckass;

    public SingleValueResolver(String paramName, Class ckass)
    {
        super(paramName);
        this.ckass = ckass;
    }


    @Override
    public Object resolve(HttpServletRequest request, HttpServletResponse response)
    {
        String value = request.getParameter(paramName);
        if (StringUtil.isNotBlank(value))
        {
            return toValue(ckass, value);
        }
        else
        {
            return null;
        }
    }
}
