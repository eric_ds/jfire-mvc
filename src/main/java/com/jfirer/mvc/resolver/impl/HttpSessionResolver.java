package com.jfirer.mvc.resolver.impl;

import com.jfirer.mvc.resolver.AbstractParamResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpSessionResolver extends AbstractParamResolver
{

    public HttpSessionResolver(String paramName)
    {
        super(paramName);
    }

    @Override
    public Object resolve(HttpServletRequest request, HttpServletResponse response)
    {
        return request.getSession();
    }
}
