package com.jfirer.mvc.resolver.impl;

import com.jfirer.baseutil.StringUtil;
import com.jfirer.baseutil.reflect.ReflectUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ListUploadResolver extends AbstractUploadBinder
{

    public ListUploadResolver(String paramName)
    {
        super(paramName);
    }


    @Override
    public Object resolve(HttpServletRequest request, HttpServletResponse response)
    {
        String contentType = request.getContentType();
        if (StringUtil.isNotBlank(contentType) && contentType.startsWith("multipart/form-data"))
        {
            try
            {
                return resolveMany(request);
            }
            catch (Exception e)
            {
                ReflectUtil.throwException(e);
                return null;
            }
        }
        else
        {
            return null;
        }
    }
}
