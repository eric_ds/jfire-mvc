package com.jfirer.mvc.resolver.impl;

import com.jfirer.baseutil.StringUtil;
import com.jfirer.baseutil.reflect.ReflectUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SingleUploadResolver extends AbstractUploadBinder
{
    private static final String EMPTY = "";

    public SingleUploadResolver(String paramName)
    {
        super(paramName.equals(EMPTY)? EMPTY:paramName);
    }

    @Override
    public Object resolve(HttpServletRequest request, HttpServletResponse response)
    {
        String contentType = request.getContentType();
        if (StringUtil.isNotBlank(contentType) && contentType.startsWith("multipart/form-data"))
        {
            if (paramName == EMPTY)
            {
                try
                {
                    return resolveOne(request);
                }
                catch (Exception e)
                {
                    ReflectUtil.throwException(e);
                }
            }
            else
            {
                try
                {
                    return resolveOne(paramName, request);
                }
                catch (Exception e)
                {
                    ReflectUtil.throwException(e);
                }
            }
        }
        else
        {
            return null;
        }
        return null;
    }
}
