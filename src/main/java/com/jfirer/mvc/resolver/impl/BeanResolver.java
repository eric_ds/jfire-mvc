package com.jfirer.mvc.resolver.impl;

import com.jfirer.mvc.binder.DataBinder2;
import com.jfirer.mvc.binder.impl.BeanBinder2;
import com.jfirer.mvc.resolver.AbstractParamResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BeanResolver extends AbstractParamResolver
{
    private DataBinder2 binder;

    public BeanResolver(String paramName, Class ckass)
    {
        super(paramName);
        binder = new BeanBinder2(paramName, ckass);
    }

    @Override
    public Object resolve(HttpServletRequest request, HttpServletResponse response)
    {
        return binder.bindMulti(request.getParameterMap());
    }
}
