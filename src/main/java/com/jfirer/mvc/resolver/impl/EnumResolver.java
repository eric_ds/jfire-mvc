package com.jfirer.mvc.resolver.impl;

import com.jfirer.mvc.binder.impl.base.EnumBinder;
import com.jfirer.mvc.resolver.AbstractParamResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EnumResolver extends AbstractParamResolver
{
    private EnumBinder binder;

    public EnumResolver(String paramName, Class ckass)
    {
        super(paramName);
        binder = new EnumBinder(paramName, ckass);
    }

    @Override
    public Object resolve(HttpServletRequest request, HttpServletResponse response)
    {
        return binder.bindMulti(request.getParameterMap());
    }
}
