package com.jfirer.mvc.resolver.impl;

import com.jfirer.mvc.binder.impl.ArrayBinder2;
import com.jfirer.mvc.resolver.AbstractParamResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ArrayResolver extends AbstractParamResolver
{
    private ArrayBinder2 binder;

    public ArrayResolver(String paramName, Class ckass)
    {
        super(paramName);
        binder = new ArrayBinder2(paramName, ckass);
    }

    @Override
    public Object resolve(HttpServletRequest request, HttpServletResponse response)
    {
        return binder.bindMulti(request.getParameterMap());
    }
}
