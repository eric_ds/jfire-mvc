package com.jfirer.mvc.resolver.impl;

import com.jfirer.mvc.binder.impl.ListBinder;
import com.jfirer.mvc.resolver.AbstractParamResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;

public class ListResolver extends AbstractParamResolver
{
    private ListBinder binder;

    public ListResolver(String paramName, Type type)
    {
        super(paramName);
        binder = new ListBinder(paramName, type);
    }

    @Override
    public Object resolve(HttpServletRequest request, HttpServletResponse response)
    {
        return binder.bindMulti(request.getParameterMap());
    }
}
