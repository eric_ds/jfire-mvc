package com.jfirer.mvc.resolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ParamResolver
{
    Object resolve(HttpServletRequest request, HttpServletResponse response);

    String paramName();
}
