package com.jfirer.mvc.annotation;

import java.lang.annotation.*;

/**
 * 这个注解表示对属性名称进行重命名工作
 * 
 * @author 林斌
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MvcRename
{
    public String value();
}
