package com.jfirer.mvc.annotation;

import java.lang.annotation.*;

/**
 * 该注解用来存放某一个方法被提取出来的说明文字
 * 
 * @author linbin
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Document
{
    public String value();
}
