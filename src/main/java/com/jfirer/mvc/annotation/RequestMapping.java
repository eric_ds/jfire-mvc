package com.jfirer.mvc.annotation;

import com.jfirer.mvc.viewrender.DefaultResultType;

import java.lang.annotation.*;

/**
 * 表示该方法是一个action方法
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestMapping
{

    enum RequestMethod
    {
        GET, POST, PUT, DELETE
    }

    /**
     * 请求路径，默认不填写的话为方法名称
     *
     * @return
     */
    String value();

    /**
     * 视图类型
     *
     * @return
     */
    String resultType() default DefaultResultType.None;

    RequestMethod[] method() default {RequestMethod.GET, RequestMethod.POST};

    /**
     * 默认为返回类型为text/html
     *
     * @return
     */
    String contentType() default "";

    /**
     * 该方法映射的别名。特定的场合下，可以用来做唯一识别
     *
     * @return
     */
    String token() default "";

    /**
     * 为这个路由规则起一个别名，不填写的情况默认使用uuid作为值
     *
     * @return
     */
    String name() default "";

    String[] headers() default {};
}
