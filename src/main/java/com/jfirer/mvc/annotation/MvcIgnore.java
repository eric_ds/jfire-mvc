package com.jfirer.mvc.annotation;

import java.lang.annotation.*;

/**
 * 这个注解表示对属性进行忽略
 * 
 * @author 林斌
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MvcIgnore
{
    
}
