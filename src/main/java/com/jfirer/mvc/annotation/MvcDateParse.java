package com.jfirer.mvc.annotation;

import java.lang.annotation.*;

/**
 * 这个注解表明将使用对应的格式转换来完成字符串到属性的转换
 * 
 * @author 林斌
 * 
 */
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MvcDateParse
{
    /**
     * 日期的转换格式
     * 
     * @return
     */
    public String date_format() default "yyyy-MM-dd HH:mm:ss";
}
