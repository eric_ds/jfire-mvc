package com.jfirer.mvc.annotation;

import com.jfirer.baseutil.bytecode.support.OverridesAttribute;

import javax.annotation.Resource;
import java.lang.annotation.*;

/**
 * 代表注解类是一个action类
 *
 * @author 林斌
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Resource(shareable = true)
public @interface Controller
{
    /**
     * Bean的名称，不指定的话为类的全限定名
     *
     * @return
     */
    @OverridesAttribute(annotation = Resource.class, name = "name") String value() default "";
}
