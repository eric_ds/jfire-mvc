package com.jfirer.mvc.annotation;

import java.lang.annotation.*;

/**
 * 方法从http请求中获取的参数
 * 
 * @author 林斌（windfire@zailanghua.com）
 * 
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RequestParam
{
    /** 该属性的名称 */
    public String value();
    
}
