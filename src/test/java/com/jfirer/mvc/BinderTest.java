package com.jfirer.mvc;

import com.jfirer.mvc.binder.DataBinder2;
import com.jfirer.mvc.binder.impl.BeanBinder2;
import com.jfirer.mvc.vo.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class BinderTest
{
    @Test
    public void test8()
    {
        Map<String, String> node = new HashMap<String, String>();
        node.put("pre[desks][0][name]", "1");
        node.put("pre[desks][1][name]", "2");
        DataBinder2 binder   = new BeanBinder2("pre", ListData.class);
        ListData    listData = (ListData) binder.bind(node);
        assertEquals("1", listData.getDesks().get(0).getName());
        assertEquals("2", listData.getDesks().get(1).getName());
        node.clear();
        Map<String, String[]> map = new HashMap<String, String[]>();
        map.put("pre[names][]", new String[]{"1", "2"});
        listData = (ListData) binder.bindMulti(map);
        assertEquals("1", listData.getNames().get(0));
        assertEquals("2", listData.getNames().get(1));
    }

    @Test
    public void test2()
    {
        BeanBinder2         binder    = new BeanBinder2(null, Desk.class);
        Map<String, String> paramTree = new HashMap<String, String>();
        paramTree.put("name", "hello");
        paramTree.put("width", "20");
        paramTree.put("l", "LONG");
        Desk desk = (Desk) binder.bind(paramTree);
        assertEquals("hello", desk.getName());
        assertEquals(20, desk.getWidth());
        Assert.assertEquals(length.LONG, desk.getL());
        paramTree.clear();
        paramTree.put("l", "LONG");
        desk = (Desk) binder.bind(paramTree);
        Assert.assertEquals(length.LONG, desk.getL());
        binder = new BeanBinder2("desk", Desk.class);
        paramTree.clear();
        paramTree.put("desk[name]", "hello");
        paramTree.put("desk[width]", "20");
        desk = (Desk) binder.bind(paramTree);
        assertEquals("hello", desk.getName());
        assertEquals(20, desk.getWidth());
    }

    @Test
    public void test3()
    {
        BeanBinder2         binder = new BeanBinder2(null, Person.class);
        Map<String, String> node   = new HashMap<String, String>();
        node.put("age", "15");
        node.put("name", "test");
        node.put("weight", "15.36");
        node.put("ids[0]", "1");
        node.put("ids[1]", "2");
        node.put("ids[2]", "3");
        Person person = (Person) binder.bind(node);
        assertEquals(15, person.getAge());
        assertEquals("test", person.getName());
        assertEquals(15.36, person.getWeight(), 0.0001);
        Integer[] ids = person.getIds();
        assertArrayEquals(new Integer[]{1, 2, 3,null,null}, ids);
    }

    @Test
    public void test4()
    {
        Map<String, String> map = new HashMap<String, String>();
        map.put("host[name]", "林斌");
        map.put("host[age]", "25");
        map.put("host[weight]", "75.26");
        map.put("host[ids][0]", "1");
        map.put("host[ids][1]", "10");
        map.put("host[ids][3]", "100");
        map.put("length", "100");
        map.put("width", "50");
        map.put("desks[0][name]", "desk1");
        map.put("desks[0][width]", "11");
        map.put("desks[1][name]", "desk2");
        map.put("desks[1][width]", "12");
        BeanBinder2 binder = new BeanBinder2(null, Home.class);
        Home        home   = (Home) binder.bind(map);
        assertEquals(home.getHost().getName(), "林斌");
        assertEquals(home.getHost().getAge(), 25);
        assertEquals(home.getHost().getWeight(), 75.26, 0.001);
        Integer[] ids = home.getHost().getIds();
        assertEquals(ids[0].intValue(), 1);
        assertEquals(ids[1].intValue(), 10);
        assertEquals(ids[3].intValue(), 100);
        Desk[] desks = home.getDesks();
        assertEquals("desk1", desks[0].getName());
        assertEquals("desk2", desks[1].getName());
        assertEquals(11, desks[0].getWidth());
        assertEquals(12, desks[1].getWidth());
    }
}
