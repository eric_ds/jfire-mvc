package com.jfirer.mvc.action;

import com.jfirer.mvc.annotation.Controller;
import com.jfirer.mvc.annotation.RequestMapping;
import com.jfirer.mvc.viewrender.DefaultResultType;
import com.jfirer.mvc.vo.Person;

@Controller
public class DemoAction
{

    @RequestMapping(value = "/person", resultType = DefaultResultType.Json)
    public Person person()
    {
        Person person = new Person();
        person.setAge(12);
        person.setWeight((float) 12.34);
        person.setName("sadasd");
        return person;
    }
}
