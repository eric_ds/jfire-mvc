package com.jfirer.mvc;

import com.jfirer.jfire.core.prepare.annotation.ComponentScan;
import com.jfirer.jfire.core.prepare.annotation.EnableAutoConfiguration;
import com.jfirer.jfire.core.prepare.annotation.configuration.Configuration;
import com.jfirer.mvc.boot.BootApplication;

@EnableAutoConfiguration
@ComponentScan("com.jfirer.mvc")
@Configuration
public class BootTest
{
    public static void main(String[] args)
    {
        BootApplication bootApplication = new BootApplication(BootTest.class,"app","/",8080);
        bootApplication.start();

    }
}
