package com.jfirer.mvc;

import com.jfirer.mvc.binder.impl.BeanBinder2;
import com.jfirer.mvc.vo.Home;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.util.HashMap;
import java.util.Map;

@State(Scope.Benchmark)
public class BinderBenchmark
{
    private Map<String, String[]> map = new HashMap<String, String[]>();
    BeanBinder2 beanBinder2 = new BeanBinder2(null, Home.class);

    public BinderBenchmark()
    {
        map.put("host[name]", new String[]{"林斌"});
        map.put("host[age]", new String[]{"25"});
        map.put("host[weight]", new String[]{"75.26"});
        map.put("host[ids][0]", new String[]{"1"});
        map.put("host[ids][1]", new String[]{"10"});
        map.put("host[ids][3]", new String[]{"100"});
        map.put("length", new String[]{"100"});
        map.put("width", new String[]{"50"});
        map.put("desks[0][name]", new String[]{"desk1"});
        map.put("desks[0][width]", new String[]{"11"});
        map.put("desks[1][name]", new String[]{"desk2"});
        map.put("desks[1][width]", new String[]{"12"});
    }

    @Benchmark
    public void testNew(Blackhole blackhole)
    {
        Object o = beanBinder2.bindMulti(map);
        blackhole.consume(o);
    }

    public static void main(String[] args) throws RunnerException
    {
        Options opt = new OptionsBuilder()//
                .include(BinderBenchmark.class.getSimpleName())//
                .measurementIterations(3)//
                .measurementTime(TimeValue.seconds(3))//
                .warmupIterations(2)//
                .warmupTime(TimeValue.seconds(3))//
                .forks(1)//
                .build();
        new Runner(opt).run();
    }
}
