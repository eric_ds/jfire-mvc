package com.jfirer.mvc;

import com.jfirer.baseutil.reflect.ReflectUtil;
import com.jfirer.jfireel.template.Template;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Demo
{
    public static void main(String[] args) throws IOException
    {
        InputStream         resourceAsStream = Demo.class.getClassLoader().getResourceAsStream("template.txt");
        int                 available        = resourceAsStream.available();
        byte[] content = new byte[available];
        resourceAsStream.read(content);
        Template            template = Template.parse(new String(content));
        Map<String, Object> map      = new HashMap<String, Object>();
        Class               ckass    = long.class;
        List<Class>         list     = new LinkedList<Class>();
        list.add(byte.class);
        list.add(short.class);
        list.add(long.class);
        list.add(boolean.class);
        list.add(float.class);
        list.add(double.class);
        list.add(char.class);
        for (Class each : list)
        {
            map.put("type", ReflectUtil.wrapPrimitive(each).getSimpleName());
//            map.put("type", each.getSimpleName());
//            map.put("type3", each.getSimpleName().substring(0, 1).toUpperCase() + each.getSimpleName().substring(1));
            System.out.println(template.render(map));
            System.out.println();
            System.out.println();
            System.out.println();

        }
    }
}
