package com.jfirer.mvc;

import com.jfirer.mvc.core.route.RouteTree;
import com.jfirer.mvc.core.route.Router;
import com.jfirer.mvc.core.route.impl.RouteTreeImpl;
import org.junit.Assert;
import org.junit.Test;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.*;

public class RouteTest
{
    @Test
    public void test()
    {
        RouteTree routeTree = new RouteTreeImpl();
        routeTree.resolve("test1", "/user/app/name", "post");
        routeTree.resolve("test2", "/user/app/tree", "post", "name=kid");
        routeTree.resolve("test3", "/user/app/tree", "get");
        routeTree.resolve("test4", "/{test}/app/tree", "get");
        routeTree.finishResolve();
        RouteRequest request;
        Router router;
        request = new RouteRequest("post", null);
        router = routeTree.route("/user/app/name", request);
        Assert.assertTrue(router.toString().indexOf("test1") != -1);
        request = new RouteRequest("post", null);
        router = routeTree.route("/user/app/tree", request);
        Assert.assertNull(router);
        request = new RouteRequest("post", "name=kidd");
        router = routeTree.route("/user/app/tree", request);
        Assert.assertNull(router);
        request = new RouteRequest("post", "name=kid");
        router = routeTree.route("/user/app/tree", request);
        Assert.assertTrue("test2", router.toString().indexOf("test2") != -1);
        request = new RouteRequest("get", "name=kid");
        router = routeTree.route("/user/app/tree", request);
        Assert.assertTrue(router.toString().indexOf("test3") != -1);
        request = new RouteRequest("get", "name=kid");
        router = routeTree.route("/usxer/app/tree", request);
        Assert.assertTrue(router.toString().indexOf("test4") != -1);
    }

    class RouteRequest implements HttpServletRequest
    {
        private final Map<String, String> headers = new HashMap<String, String>();
        private final String              method;

        public RouteRequest(String method, String headers)
        {
            this.method = method.toUpperCase();
            if (headers != null)
            {
                for (String each : headers.split("&"))
                {
                    String[] tmp = each.split("=");
                    this.headers.put(tmp[0], tmp[1]);
                }
            }
        }

        @Override
        public Object getAttribute(String name)
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Enumeration<String> getAttributeNames()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getCharacterEncoding()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void setCharacterEncoding(String env) throws UnsupportedEncodingException
        {
            // TODO Auto-generated method stub
        }

        @Override
        public int getContentLength()
        {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public long getContentLengthLong()
        {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public String getContentType()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ServletInputStream getInputStream() throws IOException
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getParameter(String name)
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Enumeration<String> getParameterNames()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String[] getParameterValues(String name)
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<String, String[]> getParameterMap()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getProtocol()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getScheme()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getServerName()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int getServerPort()
        {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public BufferedReader getReader() throws IOException
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getRemoteAddr()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getRemoteHost()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void setAttribute(String name, Object o)
        {
            // TODO Auto-generated method stub
        }

        @Override
        public void removeAttribute(String name)
        {
            // TODO Auto-generated method stub
        }

        @Override
        public Locale getLocale()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Enumeration<Locale> getLocales()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean isSecure()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public RequestDispatcher getRequestDispatcher(String path)
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getRealPath(String path)
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int getRemotePort()
        {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public String getLocalName()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getLocalAddr()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int getLocalPort()
        {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public ServletContext getServletContext()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public AsyncContext startAsync() throws IllegalStateException
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean isAsyncStarted()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean isAsyncSupported()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public AsyncContext getAsyncContext()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public DispatcherType getDispatcherType()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getAuthType()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Cookie[] getCookies()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getDateHeader(String name)
        {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public String getHeader(String name)
        {
            return headers.get(name);
        }

        @Override
        public Enumeration<String> getHeaders(String name)
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Enumeration<String> getHeaderNames()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int getIntHeader(String name)
        {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public String getMethod()
        {
            return method;
        }

        @Override
        public String getPathInfo()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getPathTranslated()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getContextPath()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getQueryString()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getRemoteUser()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean isUserInRole(String role)
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public Principal getUserPrincipal()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getRequestedSessionId()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getRequestURI()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public StringBuffer getRequestURL()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getServletPath()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public HttpSession getSession(boolean create)
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public HttpSession getSession()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String changeSessionId()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean isRequestedSessionIdValid()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean isRequestedSessionIdFromCookie()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean isRequestedSessionIdFromURL()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean isRequestedSessionIdFromUrl()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean authenticate(HttpServletResponse response) throws IOException, ServletException
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void login(String username, String password) throws ServletException
        {
            // TODO Auto-generated method stub
        }

        @Override
        public void logout() throws ServletException
        {
            // TODO Auto-generated method stub
        }

        @Override
        public Collection<Part> getParts() throws IOException, ServletException
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Part getPart(String name) throws IOException, ServletException
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public <T extends HttpUpgradeHandler> T upgrade(Class<T> handlerClass) throws IOException, ServletException
        {
            // TODO Auto-generated method stub
            return null;
        }
    }
}

